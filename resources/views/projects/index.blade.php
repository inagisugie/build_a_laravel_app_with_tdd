<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Projects</title>
    {{--    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.2/css/bulma.css">--}}
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.5/css/bulma.css">
</head>
<body>
<h1>BirdBoard</h1>
<ul>
    @foreach($projects as $project)
        <li>{{ $project->title }}</li>
        <li>{{ $project->description  }}</li>
    @endforeach
</ul>
</body>
</html>
